#!/bin/bash
set -e

[ -e editoria ] || git clone https://gitlab.coko.foundation/editoria/editoria.git
[ -e booksprints ] || git clone https://gitlab.coko.foundation/editoria/booksprints.git
[ -e editoria-templates ] || git clone https://gitlab.coko.foundation/editoria/editoria-templates.git
